// stream Missing Piece by Marika Takeuchi

var root = document.documentElement;
var CSSVar = getComputedStyle(root);

var customize_page = window.location.href.indexOf("/customize") > -1;
var on_main = window.location.href.indexOf("/customize") < 0;

document.addEventListener("DOMContentLoaded",() => {
    var bob = document.getElementsByTagName("html")[0];

    var tc = CSSVar.getPropertyValue("--TumblrControls-Color");
    bob.setAttribute("tumblr-controls",tc)
});

$(document).ready(function(){
    $(".tumblr_preview_marker___").remove();
    
    /*--------------------------*/
    
    $("body div[container]").each(function(){
        $(this).wrapInner("<div class='jqgff'><div class='uxuze'><div class='ouksk'></div></div></div>")
    })
    
    /*--------------------------*/
    
    $("[bottom-bar]").eq(0).wrapInner("<div class='ymohy'></div>");
    
    // intro block, add corner frame elements
    $("[header-block]").each(function(){
        $(this).wrapInner("<div class='vszup'></div>");
        $(this).prepend('<div class="kwotu"></div>');
        $(this).prepend('<div class="jmipp"></div>');
        $(this).prepend('<div class="omlzq"></div>');
        $(this).prepend('<div class="jepmx"></div>');
    })
    
    // get top bar height & bottom bar heights
    var jcpat = Date.now();
    var yawej = setInterval(function(){
        if(Date.now() - jcpat > 6699){
            
            clearInterval(yawej);
        } else {
            $("[top-bar-title]").each(function(){
                if($(this).height() !== "0"){
                    var vhaa = $(this).outerHeight();
                    root.style.setProperty("--TopBar-Height", vhaa + "px");
                }
            })
            
            $("[bottom-bar]").each(function(){
                if($(this).height() !== "0"){
                    var vhss = $(this).outerHeight();
                    root.style.setProperty("--BottomBar-Height", vhss + "px");
                }
            })
        }
    },0);
    
    // intro block, icon image
    $("[header-image]").each(function(){
        $(this).wrap("<div class='ahywt'><div class='peoik'></div></div>")
        
        $(this).bind("dragstart",function(){
            return false;
        });
        
        if($(this).is("[src]")){
            if($(this).attr("src") !== ""){
                var ilkqm = $(this).attr("src");
                $(this).after("<img src='" + ilkqm + "'>")
            }
        }
    })
    
    // if user wants the longbar,
    // wrap intro icon and [longbar] together
    $(".ahywt + [longbar]").each(function(){
        $(this).add($(this).prev()).wrapAll("<div class='zofnc'></div>");
    })
    
    // initialize timeline bar thing??
    $("[longbar]").each(function(){
        $(this).prepend("<div class='elbpo'></div>")
    })
    
    $(".elbpo").each(function(){
        $(this).prepend("<div class='longline'></div>");
        $(this).prepend("<div class='wydnu'></div>")
    })
    
    // longbar text flag badge thing
    $("[longbar] [text]").each(function(){
        if($(this).prevAll("[text]").length){
            $(this).remove()
        }
        
        // $(this).appendTo($(this).parents("[longbar]").find(".elbpo"))
        $(this).appendTo($(this).parents().find(".longline"))
    })
    
    // intro description
    $("[header-description]").each(function(){
        $(this).wrapInner("<div class='desc-inner'></div>")
    })
    
    // character boxes
    $("[member]").each(function(){
        $(this).wrapInner("<div box-inner></div>")
    })
    
    $("[box-inner]").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span/>")
    })
    
    // character icon (image)
    $("[member-icon]").each(function(){
        $(this).wrap("<div class='mi-bap'></div>");
        
        if($(this).is("[src]")){
            if($(this).attr("src") !== ""){
                var eofg = $(this).attr("src");
                $(this).after("<img class='vokju' src='" + eofg + "'>")
            }
        }
    })
    
    // character big blog links, if any
    $("[member-url]").each(function(){
        if($(this).attr("member-url") !== ""){
            var mbl = $(this).attr("member-url");
            $(this).parents("[member]").find(".mi-bap").append("<a member-url href=\'" + mbl + "\'></a>");
            
            if($(this).is("[hover-text]")){
                if($(this).attr("hover-text") !== ""){
                    var hvot = $(this).attr("hover-text");
                    $(this).parents("[member]").find("a[member-url]").attr("title",hvot)
                }
            }
            
            $(this).remove();
        }
    })
    
    // character big blog link, icon:
    var mi_icod = CSSVar.getPropertyValue("--Character-Link-Icon");
    if(mi_icod.indexOf('"') > -1){
        mi_icod = mi_icod.slice(1).slice(0,-1);
    }
    
    $("a[member-url]").each(function(){
        $(this).append("<i class='system-uicons' icon-name='" + mi_icod + "'></i>")
    })
    
    // character, text stuff
    $("[member] [box-inner]").each(function(){
        $(this).find(".mi-bap").nextAll().wrapAll("<div chara-text-wrap><div ztv><div chara-textarea></div></div></div>")
    })
    
    // trim </p> topbot marges
    $("[chara-textarea]").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span/>");
        
        $(this).find("p").each(function(){
            if(!$(this).prev().length){
                $(this).css("margin-top",0)
            }
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        })
    })
    
    // character label, character detail
    $("[member] [label]").each(function(){
        // if label has detail counterpart
        if($(this).next().is("[detail]")){
            $(this).add($(this).next()).wrapAll("<div stat-row></div>")
        } else {
            $(this).wrap("<div stat-row></div>")
        }
    });
    
    $("[member] [detail]").each(function(){
        if(!$(this).prev().is("[label]")){
            $(this).wrap("<div stat-row></div>")
        }
    })
    
    $("[stat-row]").each(function(){
        $(this).wrap("<div st-table></div>")
    })
    
    $("[st-table] [label]").each(function(){
        $(this).wrapInner("<span/>")
    })
    
    $("[st-table]").each(function(){
        if(!$(this).find("[label] span").length){
            $(this).css("margin-left",0)
        }
    })
    
    // remove empty <div member>s
    $("[member]").each(function(){
        if($.trim($(this).find("[box-inner]").html()) == ""){
            $(this).remove()
        }
    })
    
    /*-------- TOOLTIPS --------*/
    $("a[title]").each(function(){
        if($.trim($(this).attr("title")) !== ""){
            $(this).style_my_tooltips({
                tip_follows_cursor:true,
                tip_delay_time:0,
                tip_fade_speed:0,
                attribute:"title"
            });
        }
        
        if($(this).is("[href]")){
            if($.trim($(this).attr("href")) == ""){
                $(this).css("cursor","help")
            }
        } else {
            $(this).css("cursor","help")
        }
    })
    
    // chad
    if(!$("[custom-links]").find("a[href*='glenthemes.tumblr.com']").length){
        $("[header-links]").append("<a href=\'//glenthemes.tumblr.com\' title=\'&#x22C6; fervor &#x22C6; by glenthemes\'>credit</a>")
    }
    
    /*---- SHOW STUFF ONCE REARRANGING IS DONE ----*/
    $("[container]").addClass("uwvgk")
    
    /*---- CUSTOMIZE PAGE MESSAGE ----*/
    var vnoex = $.trim(CSSVar.getPropertyValue("--Show-Customize-Message"));
    vnoex = vnoex.toLowerCase();
    
    if(customize_page){
        if(vnoex == "yes"){
            $("body").append("<div class='vnoex'></div>");
            $(".vnoex").css({
                "position":"fixed",
                "top":"0",
                "left":"0",
                "margin-top":"22px",
                "margin-left":"-100%",
                "padding":"12px 15px",
                "background":"#fff",
                "border":"1px solid #f0f0f0",
                "border-radius":"5px",
                "box-shadow":"7px 7px 24px rgba(0,0,0,7%)",
                "width":"200px",
                "font-family":"public sans",
                "font-size":"12px",
                "line-height":"1.6",
                "color":"#222",
                "transition":"margin-left 1s ease-in-out",
                "z-index":"10",
                "opacity":"0"
            })
            
            $(".vnoex").html("Hello! Thank you for using the &#x22C6;&#8201;FERVOR&#8201;&#x22C6; page! Please read <a href=\'https://docs.google.com/presentation/d/1s2Sy731EALF--MBu-gP0sbUQ1vIXAOW7F-Aosv0YvT4/edit?usp=sharing\'>the guide</a> to get started.");
            
            $(".vnoex").css("opacity","")
            
            $(".vnoex a").css({
                "padding-bottom":"1.5px",
                "border-bottom":"2px solid #d5e4ff",
                "letter-spacing":".3px",
                "-webkit-text-stroke-width":".5px",
                "-webkit-text-stroke-color":"#538edf",
                "color":"#538edf"
            })
            
            setTimeout(function(){
                $(".vnoex").css("margin-left","22px")
            },420)
        }
    }
    
});//end ready
