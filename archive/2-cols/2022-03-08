<!-----------------------------------------------------------------------
Character Page [02]: Fervor  [compact version]
Made by glenthemes

Normal version: https://pastebin.com/raw/PmyueFWR

Initial release: 2018/01/18
Rework date: 2022/03/08
Last updated: 2022/03/08
 
TERMS OF USE:
1) Do not remove the credit.
2) Do not repost/redistribute.
3) Do not take parts of the code and use it as your own.
4) Do not use as a base code.
5) Do not mix my themes together.

HOW TO USE:
https://docs.google.com/presentation/d/1s2Sy731EALF--MBu-gP0sbUQ1vIXAOW7F-Aosv0YvT4/edit?usp=sharing

CREDITS:
(ღ˘⌣˘ღ) ~ glencredits.tumblr.com/fervor
------------------------------------------------------------------------>

<!DOCTYPE>
<html>

<head>

<title>{Title}</title>

<link rel="shortcut icon" href="{Favicon}">

<!-- jquery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

<!-- fonts -->
<link href="https://fonts.googleapis.com/css?family=Public+Sans:500|Cousine" rel="stylesheet">

<link rel="preload" href="//glen-assets.github.io/fonts/Brutal-Regular.ttf" as="font" type="font/ttf" crossorigin>

<link rel="preload" href="//glen-assets.github.io/fonts/Brutal-Medium.ttf" as="font" type="font/ttf" crossorigin>

<link rel="preload" href="//glen-assets.github.io/fonts/RussischSansSemiBold.ttf" as="font" type="font/ttf" crossorigin>

<link rel="preload" href="//glen-assets.github.io/fonts/Altone-Regular.ttf" as="font" type="font/ttf" crossorigin>

<!-- system uicons -->
<script src="//glenthemes.github.io/system-uicons/init-icons.js"></script>
<link href="//glenthemes.github.io/system-uicons/style.css" rel="stylesheet">

<!-- tooltips -->
<script src="//static.tumblr.com/snld4jp/adOr5n1p5/malihu-tooltips.js"></script>

<!-- essentials -->
<link href="//glenthemes.s3.eu-west-2.amazonaws.com/chara-pages/02/fervor.css" rel="stylesheet">

<script src="//glenthemes.s3.eu-west-2.amazonaws.com/chara-pages/02/fervor.js"></script>

<!-------------------------------------------------------------------->

<style type="text/css">

@font-face { font-family: "brutal regular"; src: url("//glen-assets.github.io/fonts/Brutal-Regular.ttf"); }

@font-face { font-family: "brutal medium"; src: url("//glen-assets.github.io/fonts/Brutal-Medium.ttf"); }

@font-face { font-family: "russisch sans"; src: url("//glen-assets.github.io/fonts/RussischSansSemiBold.ttf"); }

@font-face { font-family: "altone"; src: url("//glen-assets.github.io/fonts/Altone-Regular.ttf"); }

/*------ TOOLTIPS ------*/
#s-m-t-tooltip {
    margin:20px;
    background-color:var(--Tooltips-BG);
    font-family:var(--Tooltips-Font);
    font-size:var(--Tooltips-Text-Size);
    letter-spacing:0.5px;
    text-transform:uppercase;
    color:var(--Tooltips-Text-Color);
    z-index:99;
    max-width:40vw;
    line-height:var(--Tooltips-LineHeight);
    transition:opacity .3s ease-in-out;
}

/*------ TUMBLR CONTROLS ------*/
iframe#tumblr_controls, .iframe-controls--desktop {
    top:5px!important;
    right:5px!important;
    position:fixed!important;
    
    transform:scale(0.7,0.7);
    
    transform-origin:100% 0;
    z-index:999999!important;
}

[tumblr-controls="black"] #iframe_tumblr_controls,
[tumblr-controls="black"] .iframe-controls--desktop {
    filter:invert(100%) hue-rotate(180deg)!important;
    -webkit-filter:invert(100%) hue-rotate(180deg)!important;
}

.tmblr-iframe--follow-teaser, .follow-teaser, .iframe-controls--phone-mobile, .tmblr-iframe--app-cta-button {
    display:none!important;
    visibility:hidden!important;
    height:0!important;
}

/*------ SCROLLBAR ------*/
::-webkit-scrollbar {
    width:var(--Scrollbar-Width);
    height:var(--Scrollbar-Width);
    background-color:var(--Scrollbar-Background);
}

::-webkit-scrollbar-thumb {
    border:calc((var(--Scrollbar-Width) - 1px) / 2) solid var(--Scrollbar-Background);
    background-color:var(--Scrollbar-Thumb);
}

::-webkit-scrollbar-track {
    border:calc((var(--Scrollbar-Width) - 1px) / 2) solid var(--Scrollbar-Background);
    background-color:var(--Scrollbar-Track);
}

::-webkit-scrollbar-corner {
    background-color:var(--Scrollbar-Background);
}

/*------ BASICS ------*/
body {
    background-attachment:fixed;
    font-family:var(--Normal-Text-Font);
    font-size:var(--Normal-Text-Size);
    color:var(--Normal-Text-Color);
    line-height:var(--Normal-Text-LineHeight);
    text-align:left;
    overscroll-behavior-y:none;
}

/* 🍩💖🍩💖🍩💖🍩💖🍩 */
/*------ BACKGROUND COLOR // BACKGROUND IMAGE ------*/
body {
    background-color:#fdfdfd;
    background-image:url("https://static.tumblr.com/c0a4e7cc4f5c2882a7779a37b63132bf/2pnwama/qAaoy90vm/tumblr_static_c7ybwm57sv4k0s0kswccgoss0.png");
    background-repeat:repeat; /* repeat | no-repeat */
}

/* 🍩💖🍩💖🍩💖🍩💖🍩 */
/*------ CUSTOMIZATION OPTIONS ------*/
:root {
    /*--- DISABLE POPUP MESSAGE? ---*/
    --Show-Customize-Message:yes;
    
    /*--- TOP BAR ---*/
    --TopBar-Font:"brutal medium";
    --TopBar-Title-Size:10px;
    --TopBar-Title-Color:#eee;
    --TopBar-BG:#222;
    --TopBar-Padding:11px;
    
    /*--- MAIN OPTIONS ---*/
    --Container-Width:560px;
    --Container-FadeIn-Speed:420ms;
    
    /*--- HEADER ---*/
    --Header-Gap-Top:32px;
    --Header-Gap-Bottom:32px;
    
    --Header-Frame-Thickness:25px;
    --Header-Frame-Color:#ffffff;
    --Header-Frame-Transparency:80%; /* percentage only */
    
    --Header-Frame-Shadow-Size:40px;
    --Header-Frame-Shadow-Color:#000;
    --Header-Frame-Shadow-Transparency:95%; /* percentage only */
    
    --Header-Corner-Frame-Size:11px;
    --Header-Corner-Frame-Thickness:2px;
    --Header-Corner-Frame-Color:#222;
    
    --Header-BG:#ffffff;
    --Header-Padding:25px;
    
    --Header-Image-Border-Thickness:8px;
    --Header-Image-Border-Color:#222;
    --Header-Image-BG:transparent;
    --Header-Image-Size:80px;
    --Header-Image-Gap-Bottom:10px;
    
    --Header-LongBar-Height:16px;
    --Header-LongBar-Color:#222;
    
    --Header-LongBar-Circle-Shift:20px;
    --Header-LongBar-Circle-Size:12px;
    --Header-LongBar-Circle-Border-Thickness:8px;
    --Header-LongBar-Circle-Border-Color:#222;
    --Header-LongBar-Circle-Center-Color:#fff;
    
    --Header-LongBar-Text-Offset:25px;
    --Header-LongBar-Text-Anchor-Color:#222;
    
    --Header-LongBar-Font:"russisch sans";
    --Header-LongBar-Text-Size:9px;
    --Header-LongBar-Text-Color:#eaeaea;
    
    --Header-LongBar-Text-Padding:6px;
    --Header-LongBar-Text-BG:#222;
    
    --Header-Links-Border:#222;
    --Header-Links-BG:#222;
    --Header-Links-Padding:4px;
    --Header-Links-Font:"russisch sans";
    --Header-Links-Text-Size:8px;
    --Header-Links-Text-Color:#eee;
    --Header-Links-Spacing:4px;
    
    --Header-Links-HOVER-Border:#222;
    --Header-Links-HOVER-BG:#fff;
    --Header-Links-HOVER-Text-Color:#222;
    
    --Desc-Gap-Top:20px;
    --Desc-Gap-Left:12px;
    --Desc-Padding:14px;
    --Desc-BG:#222;
    --Desc-Font:"altone";
    --Desc-Text-Size:11px;
    --Desc-Text-Color:#e9e9e9;
    --Desc-Text-Align:left;
    --Desc-LineHeight:1.6; /* number only */
    --Desc-Links:#fff;
    --Desc-Links-Underline:#888;
    --Desc-Scrollbar:#999;
    
    /*--- ACTUAL CHARACTERS ---*/
    --Characters-Per-Row:2;
    --Character-Box-Spacing:26px;
    
    --Character-Frame-Thickness:25px;
    --Character-Frame-Color:#fff;
    --Character-Frame-Transparency:80%; /* percentage only */
    
    --Character-Frame-Shadow-Size:40px;
    --Character-Frame-Shadow-Color:#000;
    --Character-Frame-Shadow-Transparency:95%; /* percentage only */
    
    --Character-Box-Padding:12px;
    --Character-Box-BG:#fff;
    --Character-Box-Scrollbar:#aaa;
    
    --Character-Image-Size:64px;
    
    --Character-Overlay:#090909;
    --Character-Overlay-Hover-Transparency:20%; /* percentage only */
    --Character-Overlay-Fade-Speed:699ms;
    
    --Character-Link-Icon:"Eye";
    --Character-Link-Icon-Color:#e7e7e7;
    --Character-Link-Icon-Size:25px;
    
    --Character-Name-Font:"russisch sans";
    --Character-Name-Size:12px;
    --Character-Name-Color:#222;
    --Character-Name-Gap-Bottom:3px;
    
    --Character-Stat-Label-Font:"public sans";
    --Character-Stat-Label-Text-Size:9px;
    --Character-Stat-Label-Text-Color:#e6e6e6;
    --Character-Stat-Label-Padding:6px;
    --Character-Stat-Label-BG:#222;
    
    --Normal-Text-Font:"altone";
    --Normal-Text-Size:11px;
    --Normal-Text-Color:#666;
    --Normal-Text-LineHeight:1.5; /* number only */
    --Normal-Text-Links-Color:#111;
    --Normal-Text-Links-Underline:#e2e2e2;
    
    --Character-Stat-Row-Spacing:6px;
    
    /*--- BOTTOM BAR ---*/
    --BottomBar-Padding:11px;
    --BottomBar-BG:#222;
    
    --CustomLinks-Border:#666;
    --CustomLinks-Padding:5px;
    --CustomLinks-BG:transparent;
    
    --CustomLinks-Font:"brutal medium";
    --CustomLinks-Text-Size:9px;
    --CustomLinks-Text-Color:#eee;
    
    --CustomLinks-HOVER-Border:#999;
    --CustomLinks-HOVER-BG:transparent;
    --CustomLinks-HOVER-Text-Color:#eee;
    
    --CustomLinks-Spacing:8px;
    
    /*--- OTHER / MISC ---*/
    --Scrollbar-Thumb:#999;
    --Scrollbar-Track:#f2f2f2;
    --Scrollbar-Background:#fcfcfc;
    --Scrollbar-Width:13px;
    
    --Tooltips-Font:"public sans";
    --Tooltips-Text-Size:9px;
    --Tooltips-Text-Color:#e9e9e9;
    --Tooltips-LineHeight:1.4;
    --Tooltips-Padding:7px;
    --Tooltips-BG:#141414;
    
    --Highlighted-Text-BG:#f0f0f0;
    --Highlighted-Text-Color:#333;
    
    --TumblrControls-Color:black;
}

/*--- LOADING STUFF, IGNORE THIS ---*/
[container]{
    opacity:0;
}

/*------ TOP BAR TITLE ------*/
[top-bar-title]{
    position:fixed;
    top:0;
    width:var(--Container-Width);
    box-sizing:border-box;
    background:var(--TopBar-BG);
    
    font-family:var(--TopBar-Font);
    font-size:var(--TopBar-Title-Size);
    text-transform:uppercase;
    letter-spacing:0.08em;
    word-spacing:0.0420em;
    color:var(--TopBar-Title-Color);
    line-height:var(--TopBar-Title-LineHeight);
    --TopBar-Title-LineHeight:1.4;
    text-align:center;
    z-index:9;
}

/*------ MAIN CONTENT ------*/
[mainstuff]{
    margin-top:calc(var(--TopBar-Height) + var(--Header-Gap-Top));
    padding-bottom:var(--BottomBar-Height);
}

/*------ HEADER ------*/
[header-block]{
    position:relative;
    padding:var(--Header-Frame-Thickness) 0;
    margin-top:var(--Header-Gap-Top);
    width:100%;
}

/* header frame */
[header-block]:before {
    content:"";
    position:absolute;
    top:0;left:0;
    width:100%;
    height:100%;
    background:var(--Header-Frame-Color);
    opacity:calc(100% - var(--Header-Frame-Transparency));
    z-index:-1;
}

/* header shadow */
[header-block]:after {
    content:"";
    position:absolute;
    top:0;left:0;
    width:100%;
    height:100%;
    box-shadow:0 0 var(--Header-Frame-Shadow-Size) var(--Header-Frame-Shadow-Color);
    opacity:calc(100% - var(--Header-Frame-Shadow-Transparency));
    z-index:-1;
}

[header-image]{
    margin-top:var(--Header-Image-Border-Thickness);
    margin-left:var(--Header-Image-Border-Thickness);
    width:var(--Header-Image-Size);
    height:var(--Header-Image-Size);
    object-fit:cover;
}

[header-image], .ahywt {
    border-radius:100%;
}

[longbar] [text]{
    position:absolute;
    background:var(--Header-LongBar-Text-BG);
    font-family:var(--Header-LongBar-Font);
    font-size:var(--Header-LongBar-Text-Size);
    text-transform:uppercase;
    letter-spacing:0.11em;
    color:var(--Header-LongBar-Text-Color);
    text-align:left;
    line-height:var(--Header-LongBar-LineHeight);
    --Header-LongBar-LineHeight:1.3;
}

/*--- HEADER NAVLINKS ---*/
[header-links]{
    margin-top:var(--Header-Image-Gap-Bottom);
    width:calc(var(--Header-Image-Size) + (var(--Header-Image-Border-Thickness) * 2));
}

[header-links] a {
    display:block;
    border:1px solid var(--Header-Links-Border);
    background:var(--Header-Links-BG);
    font-family:var(--Header-Links-Font);
    font-size:var(--Header-Links-Text-Size);
    text-transform:uppercase;
    letter-spacing:0.169em;
    color:var(--Header-Links-Text-Color);
    line-height:var(--Header-Links-LineHeight);
    --Header-Links-LineHeight:1.3;
    text-align:center;
}

[header-links] a:hover {
    border-color:var(--Header-Links-HOVER-Border);
    background:var(--Header-Links-HOVER-BG);
    color:var(--Header-Links-HOVER-Text-Color);
}

[header-links] a + a {
    margin-top:var(--Header-Links-Spacing);
}

/*--- HEADER DESC ---*/
[header-description]{
    position:absolute;
    font-family:var(--Desc-Font);
    font-size:var(--Desc-Text-Size);
    letter-spacing:0.0269em;
    word-spacing:0.06em;
    color:var(--Desc-Text-Color);
    text-align:var(--Desc-Text-Align);
    line-height:var(--Desc-LineHeight);
    background:var(--Desc-BG);
    box-sizing:border-box;
}

[header-description] a {
    padding-bottom:1.5px;
    border-bottom:1px solid var(--Desc-Links-Underline);
    color:var(--Desc-Links);
}

/*------ CHARACTERS ------*/
[chara-grid]{
    padding:var(--Header-Gap-Bottom) 0;
    margin:calc(var(--Character-Box-Spacing) / -2);
    display:flex;
    flex-wrap:wrap;
    width:calc(100% + var(--Character-Box-Spacing));
}

[member]{
    position:relative;
    margin:calc(var(--Character-Box-Spacing) / 2);
    width:calc((100% - (var(--Character-Box-Spacing) * var(--Characters-Per-Row))) / var(--Characters-Per-Row));
}

/* character frame */
[member]:before {
    content:"";
    position:absolute;
    top:0;left:0;
    width:100%;
    height:100%;
    background:var(--Character-Frame-Color);
    opacity:calc(100% - var(--Character-Frame-Transparency));
    z-index:-1;
}

/* character shadow */
[member]:after {
    content:"";
    position:absolute;
    top:0;left:0;
    width:100%;
    height:100%;
    box-shadow:0 0 var(--Character-Frame-Shadow-Size) var(--Character-Frame-Shadow-Color);
    opacity:calc(100% - var(--Character-Frame-Shadow-Transparency));
    z-index:-1;
}

[box-inner]{
    position:relative;
    margin:var(--Character-Frame-Thickness);
    margin-right:0;
    width:calc(100% - (var(--Character-Frame-Thickness) * 2));
    background:var(--Character-Box-BG);
}

[member-icon]{
    display:block;
    width:100%;
    height:100%;
    object-fit:cover;
}

a[member-url]{
    position:absolute;
    top:0;left:0;
    display:flex;
    align-items:center;
    justify-content:center;
    width:100%;
    height:100%;
    background:var(--Character-Overlay);
    opacity:0;
    transition:opacity var(--Character-Overlay-Fade-Speed) ease-in-out;
    z-index:2;
}

[box-inner]:hover a[member-url]{
    opacity:calc(100% - var(--Character-Overlay-Hover-Transparency));
}

a[member-url] .system-uicons {
    --System-UIcons-Color:var(--Character-Link-Icon-Color);
    --System-UIcons-Size:var(--Character-Link-Icon-Size);
}

[chara-textarea]{
    padding:var(--Character-Box-Padding);
    max-height:var(--Character-Image-Size);
    box-sizing:border-box;
    overflow:auto;
    
    font-family:var(--Normal-Text-Font);
    font-size:var(--Normal-Text-Size);
    letter-spacing:0.0269em;
    color:var(--Normal-Text-Color);
    line-height:var(--Normal-Text-LineHeight);
}

[chara-textarea] p {
    margin:0.69em 0;
}

[chara-textarea] a {
    padding-bottom:1.5px;
    border-bottom:1px solid var(--Normal-Text-Links-Underline);
    color:var(--Normal-Text-Links-Color);
}

[member-name]{
    font-family:var(--Character-Name-Font);
    font-size:var(--Character-Name-Size);
    text-transform:uppercase;
    letter-spacing:0.069em;
    word-spacing:0.025em;
    color:var(--Character-Name-Color);
    line-height:var(--Normal-Text-LineHeight);
}

[stat-row] [label] span {
    background:var(--Character-Stat-Label-BG);
}

[stat-row] [label]{
    font-family:var(--Character-Stat-Label-Font);
    font-size:var(--Character-Stat-Label-Text-Size);
    text-transform:uppercase;
    letter-spacing:0.125em;
    color:var(--Character-Stat-Label-Text-Color);
}

[stat-row] [label] + [detail]{
    padding-left:5px;
}

/*------ BOTTOM BAR // CUSTOM LINKS ------*/
[bottom-bar]{
    position:fixed;
    bottom:0;
    width:var(--Container-Width);
    padding:var(--BottomBar-Padding);
    padding-bottom:calc(var(--BottomBar-Padding) - 1px);
    background:var(--BottomBar-BG);
    box-sizing:border-box;
    
    font-family:var(--CustomLinks-Font);
    font-size:var(--CustomLinks-Text-Size);
    text-transform:uppercase;
    letter-spacing:0.08em;
    word-spacing:0.0420em;
    z-index:9;
}

[bottom-bar], [custom-links] a {
    color:var(--CustomLinks-Text-Color);
}

[custom-links] a {
    display:block;
    margin:calc(var(--CustomLinks-Spacing) / 2);
    padding:var(--CustomLinks-Padding) calc(var(--CustomLinks-Padding) + 2px);
    border:1px solid var(--CustomLinks-Border);
    background:var(--CustomLinks-BG);
    line-height:1em;
}

[custom-links] a:hover {
    border-color:var(--CustomLinks-HOVER-Border);
    background:var(--CustomLinks-HOVER-BG);
    color:var(--CustomLinks-HOVER-Text-Color);
}

</style>

</head>

<body>

<div container>

<!-- 🍩💖🍩💖🍩💖🍩💖🍩 -->
<!----- TOP BAR ----->
<div top-bar-title>

    in the end, in my mind, you're the image of victory
    
</div><!--don't delete-->


<div mainstuff>

    <!----- HEADER ----->
    <div header-block>
    
        <!-- 🍩💖🍩💖🍩💖🍩💖🍩 -->
        <!-- HEADER ICON IMAGE -->
        <img header-image src="https://cdn.discordapp.com/attachments/900692000364445736/948848983369981952/Gmcd2eO.png">
        
        <div longbar>
            <!-- 🍩💖🍩💖🍩💖🍩💖🍩 -->
            <!-- HEADER LABEL TEXT -->
            <!-- the "//" at the start is optional -->
            <div text>// family & network</div>
        </div><!--don't delete-->
        
        <div header-links>
            <!-- 🍩💖🍩💖🍩💖🍩💖🍩 -->
            <!-- HEADER NAVLINKS -->
            <a href="/">index</a>
            <a href="/ask">askbox</a>
            <a href="/archive">archive</a>
        </div><!--don't delete-->
        
        <div header-description>
            <!-- 🍩💖🍩💖🍩💖🍩💖🍩 -->
            <!-- HEADER DESCRIPTION -->
            How beautiful it is to choose to be kind during difficult times. You could’ve decided to remain sour, but you favored to stay soft and kind. That shows character and humanity. Stay winning, be kind. — <a href="https://journalsbyrm.tumblr.com/post/651219377819172864">@journalsbyrm</a>
        </div><!--don't delete-->
        
    </div><!--end header // don't delete-->
    
    
    <!----- CHARACTERS ----->
    <div chara-grid>
        
        <!-- 🍩💖🍩💖🍩💖🍩💖🍩 -->
        <!-- START A NEW CHARACTER -->
        <div member no-scrollbar>
            <link member-url="https://demo.tumblr.com" hover-text="visit blog">
            <img member-icon src="https://78.media.tumblr.com/5777bf842272ebb33eda1dc59d270c3f/tumblr_p2rjjhaNqj1qg2f5co3_r2_250.png">
            
            <div member-name>Midoriya Izuku</div>
            
            <div detail>Deku</div>
        </div><!--end a character // don't delete-->
        
        <!----------->
        
        <!-- 🍩💖🍩💖🍩💖🍩💖🍩 -->
        <!-- START A NEW CHARACTER -->
        <div member no-scrollbar>
            <link member-url="https://demo.tumblr.com" hover-text="visit blog">
            <img member-icon src="https://78.media.tumblr.com/9fb61a5b0ff7c4050dfbc78f998c69d1/tumblr_p2rjjhaNqj1qg2f5co4_r1_250.png">
            
            <div member-name>Bakugou Katsuki</div>
            
            <div detail>Dynamight</div>
            
        </div><!--end a character // don't delete-->
        
        <!----------->
        
        <!-- 🍩💖🍩💖🍩💖🍩💖🍩 -->
        <!-- START A NEW CHARACTER -->
        <div member no-scrollbar>
            <link member-url="https://demo.tumblr.com" hover-text="visit blog">
            <img member-icon src="https://78.media.tumblr.com/b6d9add0ab5d06f915857a3d45e80e7f/tumblr_p2rjjhaNqj1qg2f5co5_r1_250.png">
            
            <div member-name>Uraraka Ochako</div>
            
            <div detail>Uravity</div>
        </div><!--end a character // don't delete-->
        
        <!----------->
        
        <!-- 🍩💖🍩💖🍩💖🍩💖🍩 -->
        <!-- START A NEW CHARACTER -->
        <div member no-scrollbar>
            <link member-url="https://demo.tumblr.com" hover-text="visit blog">
            <img member-icon src="https://78.media.tumblr.com/4ef71b02d0f7238c5c9bf45af25d2750/tumblr_p2rjjhaNqj1qg2f5co6_r1_250.png">
            
            <div member-name>Torodoroki Shouto</div>
            
            <div detail>Shouto</div>
        </div><!--end a character // don't delete-->
        
        <!----------->
        
        <!-- 🍩💖🍩💖🍩💖🍩💖🍩 -->
        <!-- START A NEW CHARACTER -->
        <div member no-scrollbar>
            <link member-url="https://demo.tumblr.com" hover-text="visit blog">
            <img member-icon src="https://78.media.tumblr.com/8acd892c1dddd961914e072d9fddb6b1/tumblr_p2rjjhaNqj1qg2f5co7_r1_250.png">
            
            <div member-name>Asui Tsuyu</div>
            
            <div detail>Froppy</div>
        </div><!--end a character // don't delete-->
        
    </div><!--end ALL characters // don't delete-->
</div><!--end mainstuff // don't delete-->

<!------------------------------------------->

<!----- BOTTOM BAR // CUSTOM LINKS ----->
<div bottom-bar custom-links>

    <!-- 🍩💖🍩💖🍩💖🍩💖🍩 -->
    <!-- you can edit, add, or remove any links that you like -->
    <!-- don't remove the credit! -->
    <a href="https://youtu.be/6opktP4VBN0">link one</a>
    <a href="https://youtu.be/6opktP4VBN0">link two</a>
    <a href="https://youtu.be/6opktP4VBN0">link three</a>
    <a href="https://youtu.be/6opktP4VBN0">link four</a>
    <a href="https://youtu.be/6opktP4VBN0">link five</a>
    <a href="https://youtu.be/6opktP4VBN0">link six</a>
    
    <!-- credit -->
    <a href="//glenthemes.tumblr.com" title="&#x22C6; fervor &#x22C6; by glenthemes">credit</a>
</div><!--don't delete-->
    
</div><!--don't delete-->

</body>

</html>
